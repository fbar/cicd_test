#!/bin/bash

# source configure

function die()
{
	echo >&2 "*** $0: $*, abort."
	exit 1
}

function error()
{
	echo >&2 "*** $0: $*"
}

bad=0


if ! cmp a.txt b.txt
then
	error "compare failed"
	let bad++;
fi


[ -d "$DATA_FOLDER" ] || error "Directory $DATA_FOLDER does not exist!"

if [ -z "$(ls -A $DATA_FOLDER)" ]; then
   echo "Empty"
else
   echo "Not Empty"
fi

if [ $bad -gt 0 ]
then
    die "$bad errors found"
fi





